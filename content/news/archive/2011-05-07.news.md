+++
date = "2011-05-07"
title = "Release of DUNE 2.1"
+++

The release includes the four core modules dune-common, dune-grid, dune-istl and dune-localfunctions; and the tutorials dune-grid-howto and dune-grid-dev-howto.

For a list of changes have a look at the [release notes](releases/2.1.0).
