+++
date = "2015-07-18"
title = "Dune 2.4.0-rc1 Released"
+++

The first release candidate for the upcoming 2.4 release of the core modules is now available. You can download the tarballs from our [download](/releases/) page, checkout the v2.4.0-rc1 tag of the modules via git, or get prebuilt packages from Debian experimental. Please go and test, and report the problems that you encounter.
