+++
date = "2014-03-19"
title = "UG released as free software"
+++

We are happy to announce that the UG software system has been released as free software. The copyright holders have consented to make UG available under the GNU Lesser General Public License 2.1, or (optionally) a later version. Many thanks to them!

An LGPL-licensed UG means that it is now much easier to obtain the UG source code. As of today we will not provide patch files anymore. Instead, you can go to the new [UG homepage](http://www.iwr.uni-heidelberg.de/frame/iwrwikiequipment/software/ug) directly, and get the complete source code there. That source code already contains all the latest patches needed for the use with Dune. We still provide our [installation instructions]($(ROOT)/external_libraries/install_ug.html) that show how to build UG for use with Dune.

We hope that the new license will encourage more people to try out Dune with UGGrid. We will be happy to hear your feedback.

Together with the licensing change we announce the release of a new stable version ug-3.10.0. It fixes many small memory issues, mainly related to parallel processing. The release is available from the new [homepage](http://www.iwr.uni-heidelberg.de/frame/iwrwikiequipment/software/ug).
