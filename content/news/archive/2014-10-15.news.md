+++
date = "2014-10-15"
title = "dune-grid-glue in Debian Testing"
+++

The [dune-grid-glue](/modules/dune-grid-glue/) module for the coupling of Dune grids is now available in form of binary packages for [Debian Testing/Jessie](https://packages.debian.org/search?keywords=dune-grid-glue).
