+++
date = "2010-01-15"
title = "New ALUGrid Version"
+++

ALUGrid version 1.20 has been released. Boundary segment indices and boundary projection for curved boundaries are now supported. Also, some bug fixes have been done, for example, a memory leak when using vertex or edge communication for parallel computations. The new version is available from the [ALUGrid page](http://www.mathematik.uni-freiburg.de/IAM/Research/alugrid/).
