+++
date = "2012-01-04"
title = "dune 2.1.1 maintenance release"
+++

We have just released the 2.1.1 versions of the core modules dune-grid, dune-istl, dune-localfunctions, dune-grid-howto, and dune-grid-dev-howto. They contain a fix for the bug concerning the nullptr test that has molested users of g++-4.6. Together with the previously released dune-common-2.1.1 module these modules now configure and compile with g++-4.6.

The new version is a maintenance release. It does not offer any new features or interface changes, but fixes various important bugs in dune-istl.

We wish you a happy new year where all bugs are easy to find!
