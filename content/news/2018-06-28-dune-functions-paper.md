+++
date = "2018-06-28"
title = "dune-functions paper"
tags = [ "publications", "dune-functions" ]
+++

The [function space bases in the dune-functions module][bases paper]
are described in a recently finished [article][bases paper]
covering the theoretical concepts, the user interface,
and a detailed commented example application. Please
consider citing our articles describing the
[functions][functions paper] and the [global bases][bases paper]
interface when using dune-functions in your projects
and publications.

[functions paper]: https://arxiv.org/abs/1512.06136
[bases paper]: https://arxiv.org/abs/1806.09545
