+++
date = "2018-07-17"
title = "dune-python 2.6.0 paper"
tags = [ "publications", "python" ]
+++

The release of the [dune-python] dune-python module is accompanied by a recently finished
[article][dune-python paper] describing the mechanisms behind dune-python and
providing a detailed introduction into its use.

[dune-python]: /modules/dune-python
[dune-python paper]: https://arxiv.org/abs/1807.05252
