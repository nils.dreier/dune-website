+++
module = "dune-testtools"
branch = "master"
path = "doc/sphinx/html"
title = "dune-testtools documentation"
cmakeflags = "-D DUNE_PYTHON_VIRTUALENV_SETUP=1 -D DUNE_PYTHON_ALLOW_GET_PIP=1"
[menu.main]
parent = "buildsystem"
weight = 99
+++
