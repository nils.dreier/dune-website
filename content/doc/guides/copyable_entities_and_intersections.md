+++
title = "How to port a grid to copyable entities and intersections"
[main.menu]
parent = "guides"
+++

In Dune 2.4, the `EntityPointer` has been deprecated and a number of
interface objects have become copyable and / or default-constructible.
This page contains a list of the required changes for existing grids to
help grid maintainers:

-   Entities and intersections must be copyable (and, if at all
    possible, also movable). Here, it is especially important to make
    sure that internal state doesn't get shared between copies by
    accident and that **no** state is kept in the EntityPointer,
    EntityIterator or IntersectionIterator. There are new tests in the
    grid check that verify these requirements.

-   EntityIterators and IntersectionIterators must be
    default-constructible (by extension, this also includes
    the EntityPointer). Default-constructibility makes those iterators
    more compatible with STL iterators.

-   Entities and intersections must be default-constructible. The exact
    semantics were voted on in [Doodle
    29](http://users.dune-project.org/doodles/29):\
    Entities and intersections are default-constructible. A
    default-constructed entity is invalid. Invalid entities may be
    copied around, moved and compared, but using them in any other
    context leads to undefined behavior. If a valid entity is copied /
    moved into a invalid entity, the target entity becomes valid and
    vice versa. An invalid entity always compares false to all valid
    entities, and all invalid entities compare as equal. Intersections
    behave the same.

-   All methods that currently return an EntityPointer have to return an
    Entity instead. **It is extremely important to return a fully
    constructed facade class from you implementation, otherwise the
    backwards compatibility helpers in the facades will break!** So in
    YaspGrid you must return a `Dune::Entity<...,YaspEntity>` and not a
    naked `YaspEntity`. Keep this in mind, as a lot of grid
    implementations used to rely on the facade to automatically add
    the wrapper. The following methods have to be changed:
    -   `Entity::father()` and `Entity::subEntity<codim>()` for entities
        of codimension 0
    -   `Intersection::inside()` and `Intersection::outside()`
    -   `Grid::entityPointer(const EntitySeed&)`. This method has to be
        deprecated and you must add a new method
        `Grid::entity(const EntitySeed&)` that returns an entity object

-   Depending on your performance trade-offs, your entity and
    intersection iterators may now return either a const reference to an
    internally stored entity / intersection instance (good if those
    objects are expensive) or a newly constructed temporary (good for
    meta grids).

-   **Important**: While most of the infrastructure continues to work
    for non-ported grids, meta grids like `IdentityGrid` and
    `GeometryGrid` can only be used with host grids that provide the
    new interface.

-   Until your grid has been ported, you can add the define
    `-DDUNE_GRID_CHECK_USE_DEPRECATED_ENTITY_AND_INTERSECTION_INTERFACE`
    to your gridcheck targets. That way, the grid check will be compiled
    with the old interface (and create lots and lots of
    deprecation warnings).

Meta grids
----------

If you are porting a meta grid, you should probably invest a little more
effort to benefit from the meta grid specific improvements like being
able to return temporaries and not having to juggle around with
EntityPointers inside your meta entities. Take a look at the [changes for IdentityGrid](https://gitlab.dune-project.org/core/dune-grid/commit/5a3f950f67514ab9c5d13b384e945e164d5daaae)
for inspiration.
