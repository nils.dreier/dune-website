+++
title = "Using Dune and its Python bindings through Docker/Vagrant or from source"
[menu.main]
parent = "beginnersresources"
+++

There are a number of ways to use the Python bindings for DUNE and DUNE-FEM.
The easiest way is to use the *developer's* docker image. It suffices to
execute a [script](https://gitlab.dune-project.org/dune-fem/dune-fem-dev/raw/master/rundocker.sh)
in your working directory to get the full Dune environment up and running -
then simply type
```
python myscript.py
```
and do the editing and post-processing of your data on your host as usual.
The docker image contains the git repositories of all core modules as well as
some ``dune-fem`` modules. This makes it easy to update modules or switch
to other branches. Additional modules can be easily included by simply
cloning them into the attached docker volume. Consequently, this approach
is also an easy way of developing new Dune modules (using C++).
Python runs in a virtual environment so additional packages can be easily added using ``pip``.

A source installation of the DUNE Python package is also straightforward especially if you have
familiarized with the [dune build system](/doc/installation). In addition
to your favorite DUNE modules you only need in addition [dune-python](/modules/dune-python).

For more details on

- using [Docker](/sphinx/content/sphinx/dune-fem/installation.html#using-docker-or-vagrant)
- using [Vagrant](/sphinx/content/sphinx/dune-fem/installation.html#using-docker-or-vagrant)
- installing from [source](/sphinx/content/sphinx/dune-fem/installation.html#from-source)
