+++
# The name of the module.
module = "dune-vtk"

# Groups that this module belongs to, please specify (otherwise your module will not
# be reachable from the menu through the groups) as a list.
# Currently recognized groups: "core", "disc", "grid", "extension", "user", "tutorial"
group = ["user"]

# List of modules that this module requires
requires = ["dune-grid"]

# List of modules that this module suggests
suggests = ["dune-spgrid","dune-polygongrid","dune-functions"]

# A string with maintainers to be shown in short description, if present.
maintainers = "[Simon Praetorius](mailto:simon.praetorius@tu-dresden.de)"

# Main Git repository, uncomment if present
git = "https://github.com/spraetor/dune-vtk"

# Short description (like one sentence or two). For a more detailed description,
# just write as much as you want in markdown below, uncomment if present.
short = "File reader and writer for the VTK Format"

# Doxygen documentation: Please specify the following keys to automatically build
# a doxygen documentation for this module. Note, that specifying the git key is
# necessary in this case. All of the keys should be lists of the same length.
# Each entry of the list specifies the parameter for a separate piece of documentation.
# You can use this feature to generate documentation for several branches.
#
# Specify the url, where to build the doxygen documentation
#doxygen_url = ["doxygen/mymodule"]
# Specify the branch from which to build, omit to build from master
#doxygen_branch = ["master"]
# Specify to build a a joint documentation from the following list of modules,
# omit, to build a doxygen documentation only for this module. This list will
# be used for all documentations, no list of lists necessary...
#doxygen_modules = []
# Please specify the name of the doxygen documentation, that will be shown on the main page.
#doxygen_name = ["Dune UNSTABLE"]

# Please add as many information as you want in markdown format directly below this frontmatter.
+++
This module provides structured and unstructured file writers for the VTK XML File Formats
that can be opened in the popular [ParaView](www.paraview.org) visualization application.
Additionally a file reader is provided to import VTK files into Dune grid and data objects.

Compared to the classical Dune-Grid VTKWriter it supports some new features:

* User-selected floating point precision
* Writing StructuredGrid, RectilinearGrid, and ImageData
* Compressed (appended) binary format
* Writing parametrized elements with quadratic interpolation

The VtkReader supports UnstructuredGrid files in *ASCII*, *BINARY*, or *COMPRESSED* format and
can be used in combination with a GridFactory.
