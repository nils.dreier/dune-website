+++
title = "Solving Richards Equation"
content = "carousel"
image = ["/img/dorie.jpg"]
+++

Narrow water flux entering a heterogeneous unsaturated medium.
Simulated with the DUNE based richards solver
[DORiE](http://ts.iup.uni-heidelberg.de/research/terrestrial-systems/dorie/).

<!--more-->

The grid is adaptively refined (or coarsened) depending on the estimated error in numeric flux across a grid intersection. Visualized with Paraview.
