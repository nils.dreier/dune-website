+++
title = "Electroencephalography Forward Problem"
content = "carousel"
image = ["/img/duneuro_01.png", "/img/duneuro_02.png", "/img/duneuro_03.png"]
+++

A CutFEM discretization of the EEG forward problem ([Nüßing
2018](https://nbn-resolving.org/urn:nbn:de:hbz:6-67139436770)).
Simulated with the DUNE based [duneuro](http://duneuro.org/) toolbox.

<!--more-->

The geometry is obtained from an MRI scan of a healthy subject, the image is segmented and a level-set description is generated.
To avoid problems with creating a geometry-conforming mesh, a CutFEM method is employed which directly uses the level-set information.
Using the forward solution given by Dune, the origin of a measured potential distribution at the head surface is estimated.
Visualized using Paraview and Blender.
