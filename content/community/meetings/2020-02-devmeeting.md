+++
title = "Dune Developer Meeting 2020 in Heidelberg"
+++

Date and Location
----

The developer meeting is held on February 12-13, 2020 at [IWR](https://typo.iwr.uni-heidelberg.de/contact/) in Heidelberg.
We invite all DUNE developers and contributors to participate.

## Tentative Schedule

<table>
<tr><td>Feb. 12&nbsp;</td><td>10:00 - 12:00&nbsp; </td><td>Meeting</td></tr>
<tr><td></td><td>12:00 - 13:00&nbsp; </td><td>Lunch</td></tr>
<tr><td></td><td>13:00 - 18:00&nbsp; </td><td>Meeting</td></tr>
<tr><td></td><td>19:00-N.N.</td><td>Dinner</td></tr>
<tr><td>Feb. 13</td><td>9:00 - 12:00</td><td>Meeting</td></tr>
<tr><td></td><td>12:00 - 13:00&nbsp; </td><td>Lunch</td></tr>
<tr><td></td><td>13:00 - 18:00&nbsp; </td><td>Meeting</td></tr>
</table>

Participants
-------------

- Markus Blatt
- Jorrit (Jö) Fahlke
- Christoph Grüninger (only Feb 12)
- Lasse Hinrichsen
- Dominic Kempf
- Simon Praetorius
- Oliver Sander
- Linus Seelinger

Proposed topics
---------------

Please use the edit function to add your topics or write an email to Markus.


## General

- Christoph: I'd like to make a two-hour session to revise old merge request
  and issues. We should either decide what to do about them or close them.
- Christoph: Reduce number of core modules, cf.
  [mailing list thread](https://lists.dune-project.org/pipermail/dune-devel/2019-June/002452.html)

## dune-common

- Dominic: Discuss inclusion of (support for) a logging library
    * The current implementantion with streams does not attract much use
    * Steffen left us a library: https://gitlab.dune-project.org/staging/dune-logging
    * Other full fletched systems exist e.g. https://github.com/gabime/spdlog

## dune-geometry

## dune-grid

## dune-localfunctions

## dune-istl

## dune-functions
- Simon: Parallelization of dune-functions bases. Missing tools and utilities, e.g.
    * Iteration over all DOFs on entities
    * (global) IDs of DOFs
    * Introduction of PartitionSet in several algorithms
    * Something like Twist-Uitlities for uniquely identifying the order of DOFs
      on sub entities (also global unique)
    * ParallelIndexSet for MultiIndices
    * Maybe something like dune-grid:GlobalIndexSet but for the DOFs
    * Properly wrapped local bases for DG discretization (association of local 
      DOFs to the element and not to the sub-entities)

## Discretization modules (FEM, PDELab, ...)

