+++
date = "2018-03-27T16:40:00+01:00"
version = "2.6.0"
major_version = "2"
minor_version = "6"
patch_version = "0"
modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-grid-howto", "dune-localfunctions"]
signed = 1
title = "Dune 2.6.0"
doxygen_branch = ["v2.6.0"]
doxygen_url = ["/doxygen/2.6.0"]
doxygen_name = ["Dune Core Modules"]
doxygen_version = ["2.6.0"]
support = ["linux","windows"]
[menu]
  [menu.main]
    parent = "releases"
    weight = -1
+++

# DUNE 2.6 - Release Notes

## Module dune-common:

**This release is dedicated to Elias Pipping (1986-2017).**

- New class `IntegralRange<integral_type>` and free standing function
  `range` added, providing a feature similar to Python's `range` function:
  ```
    for (const auto &i : range(5,10))
  ```
  See core/dune-common!325

- `Dune::array` was deprecated, use `std::array` from <array> instead.
   Instead of `Dune::make_array`, use `Dune::Std::make_array`
   from dune/common/std/make_array.hh
   and instead of `Dune::fill_array` use `Dune::filledArray`
   from dune/common/filledarray.hh.`

    See core/dune-common!359

- The `DUNE_VERSION...` macros are deprecated use the new macros
  `DUNE_VERSION_GT`, `DUNE_VERSION_GTE`, `DUNE_VERSION_LTE`, and
  `DUNE_VERSION_LT` instead.

    See core/dune-common!329

- Added some additional fallback implementation to C++17 features:
  (e.g. `optional`, `conjunction`, `disjunction`)

- `makeVirtualFunction`:
  allows to easily convert any function object (e.g. lambda) to a `VirtualFunction`

    See core/dune-common!282

- Added infrastructure for explicit vectorization *(experimental)*

    We added experimental support for SIMD data types. We currently
    provide infrastructure to use [Vc](https://github.com/VcDevel/Vc)
    and some helper functions to transparently switch between scalar data
    types and SIMD data types.

- `FieldMatrix` now has experimental support for SIMD types from
  [Vc](https://github.com/VcDevel/Vc) as field types.

    See core/dune-common!121



## Module dune-geometry:

- The enum `GeometryType::BasicType` is deprecated, and will be removed after Dune 2.6.

- `VirtualRefinement` and `Refinement` now support arbitrary refinements, not
  just powers of two.  Whereever you where passing a parameter `int levels`
  (now deprecated), you should now pass a parameter `RefinementIntervals
  intervals`.  There are convenience functions `refinementIntervals(int
  intervals)` and `refinementLevels(int levels)` to construct parameters of
  type `RefinementIntervals`.

    See core/dune-geometry!51

- The class `GeometryType` has been cleaned up in major way:

    See core/dune-geometry!64 and core/dune-geometry!55

  - The class and most of its methods are now `constexpr`.

  - There are new singletons and factory functions in the namespace `Dune::GeometryTypes`. These
    are now the official way to obtain a `GeometryType`.

  - The constructor taking a `GeometryType::BasicType` and a dimension has been deprecated and wil be
    removed after the release of DUNE 2.6.

  - The assorted member functions `GeometryType::make...()` have been deprecated and will be removed
    after the release of DUNE 2.6.

- The reference element interface has had a substantial overhaul that can break backwards
  compatibility in some corner cases.

    See core/dune-geometry!52

  - `ReferenceElement` has value semantics now: You should store instances by value and can freely
    copy them around. Doing so is not more expensive than storing a const reference.

  - As a consequence of value semantics, `ReferenceElement` is default constructible now. A default
    constructed `ReferenceElement` may only be assigned another `ReferenceElement`; all other
    oeprations cause undefined behavior. Moreover, instances are now comparable and hashable to
    allow storing them in maps.

  - We have added code that tries to warn you if you are still storing a `ReferenceElement` by const
    reference; please update all those occurences.

  - The meaning of `Dune::ReferenceElement` has changed. It is not a type anymore, but an alias
    template that looks up the correct implementation for the given template arguments. For now,
    there is only a single implementation, but we expect people to come up with additional
    implementations in the future. For this reason, the syntax `Dune::ReferenceElement<ctype,dim>`
    is deprecated and will cause compilation failures in the future. If you still need access to
    that type, use `typename Dune::ReferenceElements<ctype,dim>::ReferenceElement` instead.

  - You can now directly obtain a reference element for a given geometry using the free function
    `referenceElement(geometry)`. This function should be called without any namespace qualifiers to
    enable ADL and you should normally capture the return value of the function using `auto`, but if
    you need to explicitely access the type, this is also available as
    `Dune::ReferenceElement<Geometry>`.

      In short: If you can, use the following idiom to obtain a reference element for a geometry:
      ```c++
      auto ref_el = referenceElement(geometry);
      ```

      The change to the meaning of `Dune::ReferenceElement` can break compilation if you have function
      overloads that partially specialize on it, e.g.
      ```c++
      template<typename ctype, int dim>
      void f(const Dune::ReferenceElement<ctype,dim> ref_el)
      {}
      ```
      Normally, you can just simplify this to the following code that also shows how to extract the
      missing template parameters:
     ```c++
      template<typename RefEl>
      void f(const RefEl ref_el)
      {
        using ctype = typename RefEl::CoordinateField;
        constexpr auto dim = RefEl::dimension;
      }
      ```

## Module dune-grid:

- The deprecated `EntityPointer` has been removed completely and `EntityIterator`
  no longer inherits from it.
  As a consequence, the dimension `EntityIterator::dimension`,
  `EntityIterator::codimension`, and `EntityIterator::mydimension` are gone.

- Experimental grid extensions are now always enabled:

    See core/dune-grid!155

  - The method `impl` and the type `Implementation` on the facade classes are
    always public (and documented), now.
    Warning: Implementation details may change without prior notification.
  - The method experimental grid extension `boundaryId` has been removed from the
    intersection interface. Some grid will continue providing it on their
    implementation, i.e., it may still be accessible through
    ```
    intersection.impl().boundaryId()
    ```
  - The DGF block `general` is now always available and
    the DGFWriter will always write a boundary id and can write user-defined
    boundary data, now.

- `MultipleCodimMultipleGeomTypeMapper`: The `Layout` template parameter has
  been deprecated in favor of a function object that indicates which geometry
  types to include in the mapping.  The layout function
  object is passed in the constructor, so instead of
  ```c++
  MultipleCodimMultipleGeomTypeMapper<GV, MCMGElementLayout> mapper1(gv);
  MultipleCodimMultipleGeomTypeMapper<GV, MCMGVertexLayout> mapper2(gv);
  ```
  please write
  ```c++
  MultipleCodimMultipleGeomTypeMapper<GV> mapper1(gv, mcmgElementLayout());
  MultipleCodimMultipleGeomTypeMapper<GV> mapper2(gv, mcmgVertexLayout());
  ```
  See the doxygen documentation for custom layouts and core/dune-grid!177

- The `MCMGMapper` can now be used to attach multiple dofs to each
  entity:

    See core/dune-grid!215

  - the Layout is passed into the constructor and
    returns the number of dofs to attach to the given geometry type
    ```
       MCMGLayout layout = [](GeometryType gt, int griddim) {
         return gt.dim() == griddim? 2:0;
       };
       MCMGMapper mapper(grid,layout);
    ```
    Note: the layout can still return a `bool` with `true` leading to a single dof being attached.
  - The new method `MCMGMapper::indices(entity)` returns an iterable range
    (instance of `IntegralRange<Index>`)
    with the indices of dofs attached to the given entity:
    ```
      for (const auto& i : mapper.indices(entity) )
        dof = vector[i];
    ```

- Two new method were added to the MCMGMapper:
  `size_type size(GeometryType)` and
  `const std::vector< GeometryType >& types (int codim)`
  returning the number of dofs attached to the geometry type and a vector
  with all geometry types on which dofs are attached, respectively.

    See core/dune-grid!215

- The `StructuredGridFactory` now returns a `unique_ptr` instead of a
  `shared_ptr`.  Code that relies on a `shared_ptr`
  needs to explicitly assign the return value to a `shared_ptr`
  variable.

    See core/dune-grid!212

- `SubsamplingVTKWriter` now supports arbitrary refinements, not just powers
  of two.  The old constructor taking a parameter `int levels` has been
  deprecated, you should now pass a parameter `RefinementIntervals intervals`
  instead.  There are convenience functions `refinementIntervals(int
  intervals)` and `refinementLevels(int levels)` to construct parameters of
  type `RefinementIntervals` in dune-geometry.

    See core/dune-grid!193

- `UGGrid` now supports transferring element data during load balancing.

    See core/dune-grid!172

## Module dune-istl:

- `BDMatrix` objects can now be constructed and assigned from `std::initializer_list`.

- `BDMatrix` and `BTDMatrix` now implement the `setSize` method, which allows to
  resize existing matrix objects.

- The solver infrastructure was updated to support SIMD data types (see
  current changes in `dune-common`). This allows to solve multiple systems
  simultaniously using vectorization.


## Module dune-grid-howto:


## Module dune-localfunctions:

*  The `diffOrder` value has disappeared from the `LocalBasisTraits` class.
   This value encoded the highest partial derivative order implemented by
   a local basis. Encoding this value as a compile-time parameter led to
   various problems related to the dynamic interface, mainly because it
   became part of the type of the local finite element.  At the same time,
   it was suspected that very few people ever actually used the parameter.

    More practically, two things have disappeared: the `diffOrder` member
    of the `LocalBasisTraits` class, and the 8th template parameter `dorder`
    of that class.  There is no replacement, and if you have used `diffOrder`
    then you currently have to find a way to live without it.  As mentioned
    we believe that this concerns only a very small number of people.

    If you do use `diffOrder` and you absolutely need it or something similar,
    then we'd like to hear from you.  One of the reasons why there is no
    replacement is that we couldn't really think of a good use case to begin with.

*  The `QkLocalFiniteElement` class implements second partial derivatives
   of shape functions now.

* The `clone()` method was removed from the raw (non-virtual) `LocalFiniteElement`
  implementations. If you want to copy a `LocalFiniteElement` in a portable
  way which works for raw implementations as well as for the virtual interface
  class, you have to replace `lfe.clone()` by
  `Dune::LocalFiniteElementCloneFactory<LFEType>::clone(lfe)`.
