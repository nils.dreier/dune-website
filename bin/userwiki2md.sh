#!/bin/bash

INFILE=$1
OUTFILE=$2

if test -z $OUTFILE; then
	echo Usage: $0 WIKIFILE MDFILE
	echo
	echo Please make sure pandoc and iconv are installed
	echo
	exit -1
fi

echo "converting $INFILE to $OUTFILE"
iconv -f utf8 -t latin1 $INFILE | \
	iconv -c -f utf8 -t latin1 | \
	iconv -f latin1 -t utf8 | \
	pandoc -f textile -t markdown -o $OUTFILE
